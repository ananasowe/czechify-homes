package me.ananas.czechifyhomes

import com.google.gson.JsonParser
import org.bukkit.ChatColor
import java.text.MessageFormat
import java.util.*
import kotlin.collections.HashMap


class I18nManager(val plugin: CzechifyHomesPlugin) {
	private val log = plugin.getLogger()
	private val i18nFolder = "internationalisation"

	//val locales = listOf(Locale("en", "GB"), Locale("cs", "CZ")) // Locales available in the plugin
	val locales = loadLanguages()

	//val localesList = locales.map { Locale.forLanguageTag(it) } // Locales available in the plugin
	val resourceBundles: HashMap<String, ResourceBundle> = HashMap()

	init {
		locales.forEach { locale ->
			resourceBundles.put(
				locale.toLanguageTag(),
				ResourceBundle.getBundle("$i18nFolder.messages", locale, plugin.javaClass.classLoader)
			)
		}
	}

	fun loadLanguages(): List<Locale> {
		val inputStream = plugin.getResource("$i18nFolder/languages.json")
		val jsonObject = JsonParser().parse(inputStream?.reader())
		return jsonObject.asJsonObject.entrySet().map {
			val language = it.value.asJsonObject.get("language").asString
			val country = it.value.asJsonObject.get("country").asString
			log.info("Loaded language pack for ${it.key} ${language}-${country}")
			Locale(language, country)
		}
	}

	fun getString(message: String): String {
		return getStringWithParameters(Locale.getDefault().toString(), null, message)
	}

	fun getString(locale: String, message: String): String {
		return getStringWithParameters(locale, null, message)
	}

	fun getString(locale: String, messageColour: String, message: String, vararg messageParameters: Any): String {
		return getStringWithParameters(locale, messageColour, message, *messageParameters)
	}

	fun getStringWithParameters(
		locale: String,
		messageColour: String?,
		message: String,
		vararg messageParameters: Any
	): String {
		val prefferedLanguages: ArrayList<Locale.LanguageRange> = arrayListOf();
		try {
			prefferedLanguages.addAll(Locale.LanguageRange.parse(locale.replace('_', '-'))) // Preferred languages
		} finally {
			prefferedLanguages.addAll(Locale.LanguageRange.parse("en-gb")) // English fallback
		}
		try {
			val matchingLocale = Locale.filter(prefferedLanguages, locales).first() // Get the best matching locale
			val bundle = resourceBundles.get(matchingLocale.toLanguageTag()) // Get the corresponding resource bundle
			if (bundle != null) {
				val messageColourString = messageColour ?: ""
				val bundleMessage =
					messageColourString + bundle.getString(message) // TODO: Add fallback to next matching locale if the string doesn't exist
				return MessageFormat(bundleMessage, matchingLocale).format(
					messageParameters.map { it.toString() + ChatColor.RESET.toString() + messageColourString }
						.toTypedArray()
				)
			} else {
				log.warning("No matching language bundle for $locale!")
			}
		} catch (exception: Exception) {
			log.severe("Error while formatting the internationalised message: " + exception.message)
		}
		return "";
	}

	fun clearCache() {
		ResourceBundle.clearCache()
	}
}
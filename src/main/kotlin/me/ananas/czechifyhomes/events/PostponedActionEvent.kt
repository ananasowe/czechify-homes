package me.ananas.czechifyhomes.events

import me.ananas.czechifyhomes.services.PostponedActionType
import org.bukkit.event.Cancellable
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import java.util.*

class PostponedActionEvent(
	var plannedTime: Long,
	val participents: Array<UUID>?,
	val postponedActionType: PostponedActionType,
	val action: () -> Unit
) : Event(), Cancellable {

	private var cancelled = false;
	val createdAt = Date().time

	override fun getHandlers(): HandlerList {
		return handlerList
	}

	override fun isCancelled(): Boolean {
		return this.cancelled;
	}

	override fun setCancelled(cancel: Boolean) {
		this.cancelled = cancel
	}

	fun executeAction() {
		action()
	}

	companion object {
		@JvmStatic
		val handlerList = HandlerList()
	}
}
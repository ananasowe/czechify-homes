package me.ananas.czechifyhomes

import org.bukkit.plugin.Plugin

class ExecutableAsyncTask(val function: () -> Unit) {
	fun dispatch(where: Plugin) {
		where.server.scheduler.runTaskAsynchronously(where, function)
	}
}

class ExecutableSyncTask(val function: () -> Unit) {
	fun dispatch(where: Plugin) {
		where.server.scheduler.runTask(where, function)
	}
}

fun async(runnable: () -> Unit): ExecutableAsyncTask {
	return ExecutableAsyncTask(runnable)
}

fun sync(runnable: () -> Unit): ExecutableSyncTask {
	return ExecutableSyncTask(runnable)
}
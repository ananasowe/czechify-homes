package me.ananas.czechifyhomes.commands

import com.sk89q.worldedit.bukkit.BukkitAdapter
import com.sk89q.worldguard.WorldGuard
import com.sk89q.worldguard.bukkit.WorldGuardPlugin
import com.sk89q.worldguard.protection.ApplicableRegionSet
import com.sk89q.worldguard.protection.flags.Flags
import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.async
import me.ananas.czechifyhomes.events.PostponedActionEvent
import me.ananas.czechifyhomes.services.PostponedActionType
import me.ananas.czechifyhomes.sync
import org.bukkit.*
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerTeleportEvent
import org.bukkit.inventory.ItemStack
import java.util.*

class HomeCommand(val plugin: CzechifyHomesPlugin) : CommandExecutor {
	private val log = plugin.getLogger()

	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (sender is Player) {
			if (args.isEmpty()) { // Just /home - teleport player straight away
				teleportPlayerHome(sender, "")
			} else {
				if (args.size == 1 && args[0] == "help") {
					showHelp(sender)
				} else {
					if (args[0] == "tp") {
						if (args.size == 1) {
							teleportPlayerHome(sender, "")
						} else if (args.size == 2) {
							teleportPlayerHome(sender, args[1])
						} else {
							sender.sendMessage(
								plugin.i18n.getString(
									sender.locale,
									ChatColor.RED.toString(),
									"command.error.tooManyArgumentsMaybe"
								)
							)
						}
					} else if (args[0] == "check") {
						async {
							val resultSet = queryRegionsForLocation(sender.location)
							resultSet?.forEach { protectedRegion ->
								if (protectedRegion.isOwner(WorldGuardPlugin.inst().wrapPlayer(sender))) {
									sender.sendMessage(
										plugin.i18n.getString(
											sender.locale, ChatColor.GREEN.toString(), "plot.blockClaimStatusPositive",
											ChatColor.DARK_GREEN.toString() + plugin.i18n.getString(
												sender.locale,
												"info.positive.emphasis"
											)
										)
									) // IS NOT
									return@async
								}
							}
							sender.sendMessage(
								plugin.i18n.getString(
									sender.locale, ChatColor.RED.toString(), "plot.blockClaimStatusNegative",
									ChatColor.DARK_RED.toString() + plugin.i18n.getString(
										sender.locale,
										"info.negative.emphasis"
									)
								)
							) // IS NOT
						}.dispatch(plugin)
					} else if (args[0] == "set-tp") {
						async {
							val playerLocation = sender.location
							val resultSet = queryRegionsForLocation(playerLocation)
							resultSet?.forEach { protectedRegion ->
								if (protectedRegion.isOwner(WorldGuardPlugin.inst().wrapPlayer(sender))) {
									protectedRegion.setFlag(Flags.TELE_LOC, BukkitAdapter.adapt(playerLocation))
									sender.sendMessage(
										plugin.i18n.getString(
											sender.locale,
											ChatColor.GREEN.toString(),
											"plot.homeTpChanged",
											playerLocation.x.toInt(),
											playerLocation.y.toInt(),
											playerLocation.z.toInt()
										)
									)
									return@async
								}
							}
							sender.sendMessage(
								plugin.i18n.getString(
									sender.locale,
									ChatColor.RED.toString(),
									"plot.hasToStandWithinChTp"
								)
							)
						}.dispatch(plugin)
					} else if (args[0] == "visualise") {
						async {
							val playerLocation = sender.location
							val resultSet = queryRegionsForLocation(playerLocation)
							resultSet?.forEach { protectedRegion ->
								if (protectedRegion.isOwner(WorldGuardPlugin.inst().wrapPlayer(sender))) {
									val minX = protectedRegion.minimumPoint.blockX
									val maxX = protectedRegion.maximumPoint.blockX
									val minZ = protectedRegion.minimumPoint.blockZ
									val maxZ = protectedRegion.maximumPoint.blockZ
									val groundPoints = getRectanglePoints(
										playerLocation.world,
										minX.toDouble(),
										maxX.toDouble(),
										minZ.toDouble(),
										maxZ.toDouble(),
										true
									)
									sync {
										sender.sendMessage(
											plugin.i18n.getString(
												sender.locale,
												ChatColor.GREEN.toString(),
												"plot.visualisationDisplayed"
											)
										)
										groundPoints.forEach { location ->
											sender.spawnParticle(Particle.BARRIER, location, 1)
										}
									}.dispatch(plugin)
									return@async
								}
							}
							sender.sendMessage(
								plugin.i18n.getString(
									sender.locale,
									ChatColor.RED.toString(),
									"plot.hasToStandWithin"
								)
							)
						}.dispatch(plugin)
					} else if (args[0] == "fence") {
						async {
							val playerLocation = sender.location
							val resultSet = queryRegionsForLocation(playerLocation)
							resultSet?.forEach { protectedRegion ->
								if (protectedRegion.isOwner(WorldGuardPlugin.inst().wrapPlayer(sender))) {
									val minX = protectedRegion.minimumPoint.blockX
									val maxX = protectedRegion.maximumPoint.blockX
									val minZ = protectedRegion.minimumPoint.blockZ
									val maxZ = protectedRegion.maximumPoint.blockZ
									val groundPoints = getRectanglePoints(
										playerLocation.world,
										minX.toDouble(),
										maxX.toDouble(),
										minZ.toDouble(),
										maxZ.toDouble(),
										true
									)
									sync {
										val materialData = sender.inventory.itemInMainHand.type
										var delay = 0L
										if (sender.inventory.itemInMainHand.type.isBlock && !sender.inventory.itemInMainHand.type.isAir) {
											if (getAmount(
													sender,
													materialData
												) >= 60 * 4
											) { // 60 blocks per side - 4 for corners
												sender.inventory.removeItem(ItemStack(materialData, 60 * 4))
												sender.sendMessage(
													plugin.i18n.getString(
														sender.locale,
														ChatColor.GREEN.toString(),
														"plot.fenceBeingBuilt"
													)
												)
												groundPoints.forEach { location ->
													plugin.server.scheduler.runTaskLater(plugin, Runnable {
														sender.world.getBlockAt(location).setType(materialData, true)
													}, delay)
													delay += 3
												}
											} else {
												sender.sendMessage(
													plugin.i18n.getString(
														sender.locale,
														ChatColor.RED.toString(),
														"plot.notEnoughBlocks",
														60 * 4
													)
												)
											}
										} else {
											sender.sendMessage(
												plugin.i18n.getString(
													sender.locale,
													ChatColor.RED.toString(),
													"plot.wrongFenceItem"
												)
											)
										}

									}.dispatch(plugin)
									return@async
								}
							}
							sender.sendMessage(
								plugin.i18n.getString(
									sender.locale,
									ChatColor.RED.toString(),
									"plot.mustStandWithin"
								)
							)
						}.dispatch(plugin)
					} else {
						showHelp(sender)
					}
				}
			}
		} else {
			sender.sendMessage(plugin.i18n.getString("command.playeronly"))
		}
		return true
	}

	fun showHelp(sender: CommandSender) {
		if (sender is Player) {
			sender.sendMessage(
				ChatColor.DARK_GREEN.toString() + ChatColor.BOLD.toString() +
						plugin.i18n.getString(
							sender.locale,
							"help.availableCommands"
						)
			)
			sender.sendMessage(
				ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + "/home help" + ChatColor.RESET.toString() + ChatColor.GREEN.toString() + " - " + plugin.i18n.getString(
					sender.locale,
					"help.home.displaysHelp"
				)
			)
			sender.sendMessage(
				ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + "/home tp <name/empty>" + ChatColor.RESET.toString() + ChatColor.GREEN.toString() + " - " + plugin.i18n.getString(
					sender.locale,
					"help.home.tpToSpecifiedHome"
				)
			)
			sender.sendMessage(
				ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + "/home set-tp" + ChatColor.RESET.toString() + ChatColor.GREEN.toString() + " - " + plugin.i18n.getString(
					sender.locale,
					"help.home.changesTpLocation"
				)
			)
			sender.sendMessage(
				ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + "/home visualise" + ChatColor.RESET.toString() + ChatColor.GREEN.toString() + " - " + plugin.i18n.getString(
					sender.locale,
					"help.home.visualisesPlot"
				)
			)
			sender.sendMessage(
				ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + "/home fence" + ChatColor.RESET.toString() + ChatColor.GREEN.toString() + " - " + plugin.i18n.getString(
					sender.locale,
					"help.home.buildsFence"
				)
			)
			sender.sendMessage(
				ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + "/home check" + ChatColor.RESET.toString() + ChatColor.GREEN.toString() + " - " + plugin.i18n.getString(
					sender.locale,
					"help.home.checksIfWithin"
				)
			)
		}
	}

	fun queryRegionsForLocation(location: Location): ApplicableRegionSet? {
		val container = WorldGuard.getInstance().platform.regionContainer
		val query = container.createQuery()
		return query.getApplicableRegions(BukkitAdapter.adapt(location))
	}

	fun getRectanglePoints(
		world: World?,
		minX: Double,
		maxX: Double,
		minY: Double,
		maxY: Double,
		ground: Boolean
	): MutableSet<Location> {
		val points = mutableSetOf<Location>()
		if (world != null) {
			points.addAll(interpolateLocations(world, minX, maxX, true, minY, ground))
			points.addAll(interpolateLocations(world, minY, maxY, false, maxX, ground))
			points.addAll(interpolateLocations(world, minX, maxX, true, maxY, ground).reversed())
			points.addAll(interpolateLocations(world, minY, maxY, false, minX, ground).reversed())
		}
		return points
	}

	fun interpolateLocations(
		world: World,
		val1: Double,
		val2: Double,
		horizontal: Boolean,
		valC: Double,
		ground: Boolean
	): ArrayList<Location> {
		val points = arrayListOf<Location>()
		for (p in val1.toInt()..val2.toInt()) {
			val pointX = if (horizontal) p.toDouble() else valC
			val pointY = if (horizontal) valC else p.toDouble()
			if (ground) {
				points.add(getMinimumPoint(world, pointX, pointY))
			} else {
				points.add(Location(world, pointX, world.maxHeight.toDouble() / 2, pointY))
			}
		}
		return points
	}

	fun getMinimumPoint(world: World, x: Double, z: Double): Location {
		var y = world.maxHeight.toDouble()
		while (world.getBlockAt(x.toInt(), y.toInt(), z.toInt()).isPassable) {
			y--
		}
		return Location(world, x + .5, y + 1.5, z + .5)
	}

	fun getAmount(player: Player, material: Material): Int {
		val inventory = player.inventory
		val items = inventory.contents
		var has = 0
		for (item in items) {
			if (item != null && item.type == material && item.amount > 0) {
				has += item.amount
			}
		}
		return has
	}

	fun teleportPlayerHome(player: Player, homeName: String) {
		async {
			val playerHome = plugin.dataStorage.getPlayerHomeByName(player, homeName)
			val teleportLocation = playerHome?.getTeleportLocation()
			if (playerHome != null && teleportLocation != null) {
				if (plugin.postponedActionsQueue.hasEventsOFTypeAssociatedWithUUID(
						player.uniqueId,
						arrayOf(PostponedActionType.TELEPORT)
					)
				) {
					sync {
						player.sendMessage(
							plugin.i18n.getString(
								player.locale,
								ChatColor.RED.toString(),
								"teleporation.error.alreadyAwaitingTeleportation"
							)
						)
					}.dispatch(plugin)
				} else {
					sync {
						player.sendMessage(
							plugin.i18n.getString(
								player.locale,
								ChatColor.DARK_GREEN.toString(),
								"command.teleportation.pleaseWait",
								ChatColor.GREEN.toString() + 5
							)
						)
						val initialLocation = player.location
						plugin.server.pluginManager.callEvent(
							PostponedActionEvent(
								Date().time.plus(5000),
								arrayOf(player.uniqueId),
								PostponedActionType.TELEPORT
							) {
								val targetPlayer = plugin.server.getPlayer(player.uniqueId) // Resolve player UUID
								if (targetPlayer != null) { // Check if a fresh instance created above is an existing player
									if (targetPlayer.location.distance(initialLocation) < 1) {
										targetPlayer.teleport(
											teleportLocation,
											PlayerTeleportEvent.TeleportCause.COMMAND
										)
										targetPlayer.sendMessage(
											plugin.i18n.getString(
												player.locale,
												ChatColor.GREEN.toString(),
												"command.teleportation.teleportedHome"
											)
										)
										targetPlayer.world.playEffect(targetPlayer.location, Effect.PORTAL_TRAVEL, 0, 2)
										targetPlayer.world.spawnParticle(Particle.PORTAL, targetPlayer.location, 100)
									} else {
										targetPlayer.sendMessage(
											plugin.i18n.getString(
												player.locale,
												ChatColor.RED.toString(),
												"command.teleportation.teleportCancelled"
											)
										)
									}
								}
							}
						)
					}.dispatch(plugin)
				}
			} else {
				sync {
					if (homeName.length > 0) {
						player.sendMessage(
							plugin.i18n.getString(
								player.locale,
								ChatColor.RED.toString(),
								"plot.noHomeDefined",
								homeName
							)
						)
					} else {
						player.sendMessage(
							plugin.i18n.getString(
								player.locale,
								ChatColor.RED.toString(),
								"plot.noHome"
							)
						)
					}
				}.dispatch(plugin)
			}
		}.dispatch(plugin)
	}
}

package me.ananas.czechifyhomes.commands

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.events.PostponedActionEvent
import me.ananas.czechifyhomes.services.PostponedActionType
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.util.*

class TpaCommand(val plugin: CzechifyHomesPlugin) : CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (sender is Player) {
			if (args.size == 1) {
				val teleportationTarget = plugin.server.getPlayer(args[0])
				if (teleportationTarget != null && teleportationTarget != sender) {
					if (plugin.postponedActionsQueue.hasEventsOFTypeAssociatedWithUUID(
							teleportationTarget.uniqueId,
							arrayOf(PostponedActionType.TELEPORT_REQUEST)
						)
					) {
						sender.sendMessage(
							plugin.i18n.getString(
								sender.locale,
								ChatColor.RED.toString(),
								"teleportation.stillAwaiting"
							)
						)
					} else {
						plugin.server.pluginManager.callEvent(
							PostponedActionEvent(
								Date().time.plus(60 * 1000), // Expires in one minute and informs the sender about it
								arrayOf(
									teleportationTarget.uniqueId,
									sender.uniqueId
								), // Both players are the event participents
								PostponedActionType.TELEPORT_REQUEST
							) {
								plugin.server.getPlayer(sender.uniqueId) // Resolve player UUID
									?.sendMessage(
										plugin.i18n.getString(
											sender.locale,
											ChatColor.RED.toString(),
											"teleportation.expired",
											ChatColor.GRAY.toString() + teleportationTarget.name
										)
									)
							}
						)
						teleportationTarget.sendMessage(
							ChatColor.DARK_GREEN.toString() + "The player " + ChatColor.GRAY.toString() + sender.name + ChatColor.DARK_GREEN + " would like to teleport to you! " +
									"Use " + ChatColor.GREEN.toString() + "/tpaccept" + ChatColor.DARK_GREEN.toString() + " to accept the request or " +
									ChatColor.GREEN.toString() + "/tpdeny" + ChatColor.DARK_GREEN.toString() + " to deny it! You can also just ignore the request."
						)
						sender.sendMessage(
							plugin.i18n.getString(
								sender.locale,
								ChatColor.GREEN.toString(),
								"command.tpa.requestSentTo",
								ChatColor.GRAY.toString() + teleportationTarget.name
							)
						)
					}
				} else {
					sender.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.RED.toString(),
							"command.tpa.couldNotSend"
						)
					)
				}
			} else {
				sender.sendMessage(
					plugin.i18n.getString(
						sender.locale,
						ChatColor.RED.toString(),
						"command.tpa.specifyPlayer"
					)
				)
			}
		} else {
			sender.sendMessage(plugin.i18n.getString("command.playeronly"))
		}
		return true;
	}
}
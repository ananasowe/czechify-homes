package me.ananas.czechifyhomes.commands

import com.sk89q.worldedit.bukkit.BukkitAdapter
import com.sk89q.worldedit.math.BlockVector3
import com.sk89q.worldguard.WorldGuard
import com.sk89q.worldguard.protection.flags.Flags
import com.sk89q.worldguard.protection.flags.StateFlag
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion
import me.ananas.czechifyhomes.CzechifyHomesPlugin
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class SethomeCommand(val plugin: CzechifyHomesPlugin) : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender is Player) {
            if (args.isNotEmpty()) {
                sender.sendMessage(
                    plugin.i18n.getString(
                        sender.locale,
                        ChatColor.RED.toString(),
                        "command.otherOptionsUnavailable"
                    )
                )
            } else {
                val homeRegions = plugin.dataStorage.getPlayerHomeRegions(sender)
                if (homeRegions != null && homeRegions.isNotEmpty()) {
                    sender.sendMessage(
                        plugin.i18n.getString(
                            sender.locale,
                            ChatColor.RED.toString(),
                            "plot.alreadyDefined"
                        )
                    )
                } else { // TODO: Make this asynchronous
                    val location = sender.location
                    val regions =
                        WorldGuard.getInstance().platform.regionContainer.get(BukkitAdapter.adapt(location.world))

                    val min = BlockVector3.at(location.blockX - 30, 0, location.blockZ - 30)
                    val max = BlockVector3.at(location.blockX + 30, location.world!!.maxHeight, location.blockZ + 30)
                    val playerHouseArea = ProtectedCuboidRegion("home_" + sender.name, min, max)

                    if (regions != null) {
                        val resultSet = regions.getApplicableRegions(playerHouseArea)
                        if (resultSet.size() > 0) {
                            sender.sendMessage(
                                plugin.i18n.getString(
                                    sender.locale,
                                    ChatColor.RED.toString(),
                                    "plot.overlaps"
                                )
                            )
                        } else {
                            playerHouseArea.owners.addPlayer(sender.uniqueId)
                            playerHouseArea.setFlag(
                                Flags.ENDER_BUILD,
                                StateFlag.State.DENY
                            ) // Disable destruction on plots
                            playerHouseArea.setFlag(Flags.RAVAGER_RAVAGE, StateFlag.State.DENY) // ...
                            playerHouseArea.setFlag(
                                Flags.TELE_LOC,
                                BukkitAdapter.adapt(location)
                            ) // Set teleport location
                            regions.addRegion(playerHouseArea)
                            plugin.dataStorage.addPlayerHome(sender, playerHouseArea, location, "")
                            sender.sendMessage(
                                plugin.i18n.getString(
                                    sender.locale,
                                    ChatColor.GREEN.toString(),
                                    "plot.created"
                                )
                            )
                        }
                    } else {
                        sender.sendMessage(
                            plugin.i18n.getString(
                                sender.locale,
                                ChatColor.RED.toString(),
                                "plot.actionFailed"
                            )
                        )
                    }
                }
            }
        } else {
            sender.sendMessage(plugin.i18n.getString("command.playeronly"))
        }
        return true;
    }
}

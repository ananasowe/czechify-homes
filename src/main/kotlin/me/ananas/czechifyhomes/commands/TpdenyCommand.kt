package me.ananas.czechifyhomes.commands

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.services.PostponedActionType
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class TpdenyCommand(val plugin: CzechifyHomesPlugin) : CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (sender is Player) {
			val teleportationEvent = plugin.postponedActionsQueue.getOldestEventOfTypeAssociatedWithUUID(
				sender.uniqueId,
				arrayOf(PostponedActionType.TELEPORT_REQUEST),
				true
			)
			if (teleportationEvent != null) {
				val teleportInitiatingPlayer =
					teleportationEvent.participents?.last()?.let { plugin.server.getPlayer(it) }
				if (teleportInitiatingPlayer != null && teleportInitiatingPlayer != sender) {
					teleportInitiatingPlayer.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.RED.toString(),
							"teleportation.deniedBy",
							ChatColor.GRAY.toString() + sender.name
						)
					)
					sender.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.GREEN.toString(),
							"command.tpdeny.success"
						)
					)
				} else {
					sender.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.RED.toString(),
							"command.error.unexpected"
						)
					)
				}
			} else {
				sender.sendMessage(
					plugin.i18n.getString(
						sender.locale,
						ChatColor.RED.toString(),
						"command.error.noAwaitingTeleportation"
					)
				)
			}
		} else {
			sender.sendMessage(plugin.i18n.getString("command.playeronly"))
		}
		return true;
	}
}
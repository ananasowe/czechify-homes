package me.ananas.czechifyhomes.commands

import app.ashcon.intake.Command
import org.bukkit.command.CommandSender

class HomeCommands {
	@Command(
		aliases = arrayOf("home", "teleport"),
		desc = "Teleports you home",
		perms = arrayOf("czechifyhomes.home")
	)
	fun tp(sender: CommandSender) {
		sender.sendMessage("Teleporting you home...")
	}

	@Command(
		aliases = arrayOf("help"),
		desc = "Shows help for home command",
		perms = arrayOf("czechifyhomes.help")
	)
	fun help(sender: CommandSender) {
		sender.sendMessage("That displays help")
	}

	@Command(
		aliases = arrayOf("set-teleport-location"),
		desc = "Sets the teleport location for /home tp",
		perms = arrayOf("czechifyhomes.settplocation")
	)
	fun setTeleportLocation(sender: CommandSender) {
		sender.sendMessage("New teleport location set")
	}
}
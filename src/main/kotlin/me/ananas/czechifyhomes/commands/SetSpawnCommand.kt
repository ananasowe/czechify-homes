package me.ananas.czechifyhomes.commands

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.modules.SpawnModule
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class SetSpawnCommand(val plugin: CzechifyHomesPlugin, val spawnModule: SpawnModule) : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender is Player) {
            if (args.isNotEmpty()) {
                sender.sendMessage(
                    plugin.i18n.getString(
                        sender.locale,
                        ChatColor.RED.toString(),
                        "command.error.tooManyArguments"
                    )
                )
            } else {
                val location = sender.location
                spawnModule.updateSpawnLocation(location)
                sender.sendMessage(
                    plugin.i18n.getString(
                        sender.locale,
                        ChatColor.GREEN.toString(),
                        "spawn.locationSet",
                        location.x,
                        location.y,
                        location.z
                    )
                )
            }
        } else {
            sender.sendMessage(plugin.i18n.getString("command.playeronly"))
        }
        return true;
    }
}

package me.ananas.czechifyhomes.commands

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.events.PostponedActionEvent
import me.ananas.czechifyhomes.modules.SpawnModule
import me.ananas.czechifyhomes.services.PostponedActionType
import me.ananas.czechifyhomes.sync
import org.bukkit.ChatColor
import org.bukkit.Effect
import org.bukkit.Particle
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerTeleportEvent
import java.util.*


class SpawnCommand(val plugin: CzechifyHomesPlugin, val spawnModule: SpawnModule) : CommandExecutor {
	private val log = plugin.getLogger()

	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (sender is Player) {
			if (args.isEmpty()) {
				if (plugin.postponedActionsQueue.hasEventsOFTypeAssociatedWithUUID(
						sender.uniqueId,
						arrayOf(PostponedActionType.TELEPORT)
					)
				) {
					sync {
						sender.sendMessage(
							plugin.i18n.getString(
								sender.locale,
								ChatColor.RED.toString(),
								"teleporation.error.alreadyAwaitingTeleportation"
							)
						)
					}.dispatch(plugin)
				} else {
					sync {
						sender.sendMessage(
							plugin.i18n.getString(
								sender.locale,
								ChatColor.DARK_GREEN.toString(),
								"command.teleportation.pleaseWait",
								ChatColor.GREEN.toString() + 5
							)
						)
						val initialLocation = sender.location
						plugin.server.pluginManager.callEvent(
							PostponedActionEvent(
								Date().time.plus(5000),
								arrayOf(sender.uniqueId),
								PostponedActionType.TELEPORT
							) {
								val targetPlayer = plugin.server.getPlayer(sender.uniqueId) // Resolve player UUID
								if (targetPlayer != null) { // Check if a fresh instance is an existing player
									if (targetPlayer.location.distance(initialLocation) < 1) {
										targetPlayer.teleport(
											spawnModule.getSpawnLocation(),
											PlayerTeleportEvent.TeleportCause.COMMAND
										)
										targetPlayer.sendMessage(
											plugin.i18n.getString(
												sender.locale,
												ChatColor.GREEN.toString(),
												"command.teleportation.teleportedToSpawn"
											)
										)
										targetPlayer.world.playEffect(targetPlayer.location, Effect.PORTAL_TRAVEL, 0, 2)
										targetPlayer.world.spawnParticle(Particle.PORTAL, targetPlayer.location, 100)
									} else {
										targetPlayer.sendMessage(
											plugin.i18n.getString(
												sender.locale,
												ChatColor.RED.toString(),
												"command.teleportation.teleportCancelled"
											)
										)
									}
								}
							}
						)
					}.dispatch(plugin)
				}
			} else {
				sender.sendMessage(
					plugin.i18n.getString(
						sender.locale,
						ChatColor.RED.toString(),
						"command.error.tooManyArguments"
					)
				)

			}
		} else {
			sender.sendMessage(plugin.i18n.getString("command.playeronly"))
		}
		return true
	}
}

package me.ananas.czechifyhomes.commands

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.events.PostponedActionEvent
import me.ananas.czechifyhomes.services.PostponedActionType
import org.bukkit.ChatColor
import org.bukkit.Effect
import org.bukkit.Particle
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerTeleportEvent
import java.util.*

class TpacceptCommand(val plugin: CzechifyHomesPlugin) : CommandExecutor {
	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (sender is Player) {
			val teleportationEvent = plugin.postponedActionsQueue.getOldestEventOfTypeAssociatedWithUUID(
				sender.uniqueId,
				arrayOf(PostponedActionType.TELEPORT_REQUEST),
				true
			)
			if (teleportationEvent != null) {
				val teleportInitiatingPlayer =
					teleportationEvent.participents?.last()?.let { plugin.server.getPlayer(it) }
				if (teleportInitiatingPlayer != null && teleportInitiatingPlayer != sender) {
					val initialLocation = teleportInitiatingPlayer.location
					plugin.server.pluginManager.callEvent(
						PostponedActionEvent(
							Date().time.plus(5000),
							arrayOf(sender.uniqueId),
							PostponedActionType.TELEPORT
						) {
							val targetPlayer =
								plugin.server.getPlayer(teleportInitiatingPlayer.uniqueId) // Resolve player UUID
							val teleportationTargetPlayer =
								plugin.server.getPlayer(sender.uniqueId) // Resolve target player UUID
							if (targetPlayer != null && teleportationTargetPlayer != null) { // Check if fresh instances created above are existing players
								if (targetPlayer.location.distance(initialLocation) < targetPlayer.walkSpeed) {
									targetPlayer.teleport(
										teleportationTargetPlayer.location,
										PlayerTeleportEvent.TeleportCause.COMMAND
									)
									targetPlayer.sendMessage(
										plugin.i18n.getString(
											sender.locale,
											ChatColor.GREEN.toString(),
											"teleportation.youGotTeleportedTo",
											ChatColor.GRAY.toString() + teleportationTargetPlayer.name
										)
									)
									teleportationTargetPlayer.sendMessage(
										plugin.i18n.getString(
											sender.locale,
											ChatColor.GREEN.toString(),
											"teleportation.playerIsHere",
											ChatColor.GRAY.toString() + targetPlayer.name
										)
									)
									targetPlayer.world.playEffect(targetPlayer.location, Effect.PORTAL_TRAVEL, 0, 2)
									targetPlayer.world.spawnParticle(Particle.PORTAL, targetPlayer.location, 100)
								} else {
									targetPlayer.sendMessage(
										plugin.i18n.getString(
											sender.locale,
											ChatColor.RED.toString(),
											"command.teleportation.teleportCancelled"
										)
									)
									teleportationTargetPlayer.sendMessage(
										plugin.i18n.getString(
											sender.locale,
											ChatColor.RED.toString(),
											"teleportation.cancelledCause",
											ChatColor.GRAY.toString() + targetPlayer.name
										)
									)
								}
							}
						}
					)
					teleportInitiatingPlayer.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.WHITE.toString(),
							"teleportation.acceptedBy",
							ChatColor.GRAY.toString() + sender.name,
							ChatColor.GREEN.toString() + 5
						)
					)
					sender.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.DARK_GREEN.toString(),
							"teleportation.accepted",
							ChatColor.DARK_GREEN,
							sender.locale,
							ChatColor.GRAY.toString() + teleportInitiatingPlayer.name,
							ChatColor.GREEN.toString() + 5
						)
					)
				} else {
					sender.sendMessage(
						plugin.i18n.getString(
							sender.locale,
							ChatColor.RED.toString(),
							"command.error.unexpected"
						)
					)
				}
			} else {
				sender.sendMessage(
					plugin.i18n.getString(
						sender.locale,
						ChatColor.RED.toString(),
						"command.error.noAwaitingTeleportation"
					)
				)
			}
		} else {
			sender.sendMessage(plugin.i18n.getString("command.playeronly"))
		}
		return true;
	}
}
package me.ananas.czechifyhomes

import org.bukkit.configuration.InvalidConfigurationException
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.java.JavaPlugin
import java.io.*
import java.util.*

object Config {
	private val configs: MutableList<RConfig> = ArrayList<RConfig>()
	fun registerConfig(id: String, fileName: String?, plugin: JavaPlugin): Boolean {
		if (fileName != null) {
			val file = File(plugin.dataFolder, fileName)
			if (!file.exists()) {
				file.parentFile.mkdirs()
				try {
					copy(plugin.getResource(fileName), file)
				} catch (localException: Exception) {
				}
			}
			val c = RConfig(id, file)
			for (x in configs) {
				if (x.equals(c)) {
					return false
				}
			}
			configs.add(c)
			return true
		} else {
			return false
		}
	}

	fun unregisterConfig(id: String?): Boolean {
		return configs.remove(getConfig(id))
	}

	fun getConfig(id: String?): RConfig? {
		for (c in configs) {
			if (c.configId.equals(id, ignoreCase = true)) {
				return c
			}
		}
		return null
	}

	fun save(id: String): Boolean {
		val c = getConfig(id) ?: return false
		try {
			c.save()
		} catch (e: Exception) {
			print("An error occurred while saving a config with id $id")
			e.printStackTrace()
			return false
		}
		return true
	}

	fun saveAll(): Boolean {
		try {
			for (c in configs) {
				c.save()
			}
		} catch (e: Exception) {
			print("An error occurred while saving all configs")
			e.printStackTrace()
			return false
		}
		return true
	}

	fun load(id: String): Boolean {
		val c = getConfig(id) ?: return false
		try {
			c.load()
		} catch (e: Exception) {
			print("An error occurred while loading a config with id $id")
			e.printStackTrace()
			return false
		}
		return true
	}

	fun loadAll(): Boolean {
		try {
			for (c in configs) {
				c.load()
			}
		} catch (e: Exception) {
			print("An error occurred while loading all configs")
			e.printStackTrace()
			return false
		}
		return true
	}

	fun clear(id: String?) {
		val c = getConfig(id) ?: return
		configs.remove(c)
		configs.add(RConfig(c.configId, c.file))
	}

	private fun print(msg: String) {
		println("Config: $msg")
	}

	@Throws(IOException::class)
	private fun copy(`in`: InputStream?, file: File) {
		val out: OutputStream = FileOutputStream(file)
		val buf = ByteArray('a'.toInt())
		var len: Int
		while (`in`!!.read(buf).also { len = it } > 0) {
			out.write(buf, 0, len)
		}
		out.close()
		`in`.close()
	}

	class RConfig(val configId: String, val file: File) : YamlConfiguration() {
		@Throws(IOException::class)
		fun save() {
			save(file)
		}

		@Throws(InvalidConfigurationException::class, FileNotFoundException::class, IOException::class)
		fun load() {
			load(file)
		}

		override fun equals(other: Any?): Boolean {
			if (other is RConfig) {
				return other.configId.equals(configId, ignoreCase = true)
			} else {
				return super.equals(other)
			}
		}
	}
}
package me.ananas.czechifyhomes.listeners

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.events.PostponedActionEvent
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import java.text.DateFormat
import java.util.*

class PostponedActionListener(val plugin: CzechifyHomesPlugin) : Listener {
	private val log = plugin.logger

	@EventHandler
	fun onPostponedActionEventCreated(event: PostponedActionEvent) {
		log.fine(
			"Postponed event added to the queue (to be executed at " + DateFormat.getDateTimeInstance(
				DateFormat.SHORT,
				DateFormat.SHORT,
				Locale("pl", "PL")
			).format(event.plannedTime) + ")"
		)
		plugin.postponedActionsQueue.queue(event.plannedTime, event)
	}
}
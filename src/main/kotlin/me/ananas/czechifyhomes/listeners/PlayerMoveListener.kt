package me.ananas.czechifyhomes.listeners

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.services.PostponedActionType
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent

class PlayerMoveListener(val plugin: CzechifyHomesPlugin) : Listener {

	@EventHandler
	fun onPlayerMove(event: PlayerMoveEvent) {
		val player = event.player
		if (plugin.postponedActionsQueue.cancelActionsForFirstAssociatedUUID(
				player.uniqueId,
				PostponedActionType.TELEPORT
			) > 0
		) {
			player.sendMessage(ChatColor.RED.toString() + "Your teleportation request has been cancelled!")
		}
	}
}
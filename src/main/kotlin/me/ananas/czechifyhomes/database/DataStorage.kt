package me.ananas.czechifyhomes.database

import com.sk89q.worldguard.protection.regions.ProtectedRegion
import me.ananas.czechifyhomes.objects.Home
import org.bukkit.Location
import org.bukkit.entity.Player

interface DataStorage {
	fun prepare()
	fun shutdown()

	fun getPlayerHomeByName(player: Player, homeName: String?): Home?
	fun getPlayerHomes(player: Player): List<Home>?

	// Backend
	fun addPlayerHome(player: Player, region: ProtectedRegion, teleportLocation: Location, homeName: String)
	fun getPlayerHomeRegions(player: Player): List<ProtectedRegion>?
	fun getRegionOwner(region: ProtectedRegion): Player?
	fun getPlayerHomeRegionByName(player: Player, homename: String): ProtectedRegion?
}
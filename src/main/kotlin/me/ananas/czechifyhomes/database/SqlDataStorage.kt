package me.ananas.czechifyhomes.database

import com.sk89q.worldedit.bukkit.BukkitAdapter
import com.sk89q.worldguard.WorldGuard
import com.sk89q.worldguard.protection.regions.ProtectedRegion
import me.ananas.czechifyhomes.objects.Home
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import java.util.*

class SqlDataStorage(val sqlDatabase: SqlDatabase, val plugin: Plugin) : DataStorage {
	private val log = plugin.getLogger()

	var CREATE_TABLE =
		"CREATE TABLE IF NOT EXISTS {prefix}homes (id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, player_uuid VARCHAR(255), region_id VARCHAR(255), home_name VARCHAR(255), x INT, y INT, z INT, world VARCHAR(255))"
	var ADD_PLAYER_HOME_REGION = "INSERT INTO {prefix}homes VALUES (null, ?, ?, ?, ?, ?, ?, ?)"
	var GET_PLAYER_HOME_REGIONS_BY_PLAYER_ID =
		"SELECT world, region_id FROM {prefix}homes WHERE player_uuid = ? ORDER BY id DESC"
	var GET_PLAYER_HOME_REGION_BY_NAME =
		"SELECT region_id, world, x, y, z FROM {prefix}homes WHERE player_uuid = ? AND home_name = ? ORDER BY id DESC"
	var GET_REGION_OWNER = "SELECT player_uuid FROM {prefix}homes WHERE region_id = ?"

	val processQuery = { input: String -> input.replace("{prefix}", sqlDatabase.table_prefix) }

	override fun prepare() {
		sqlDatabase.getConnection().use {
			if (it != null && !it.isClosed()) {
				val statement = it.prepareStatement(processQuery(CREATE_TABLE))
				statement.executeUpdate()
			}
		}
	}

	override fun shutdown() {
		sqlDatabase.disconnect()
	}

	override fun getPlayerHomeByName(player: Player, homeName: String?): Home? {
		sqlDatabase.getConnection().use {
			if (it != null && !it.isClosed()) {
				val statement = it.prepareStatement(processQuery(GET_PLAYER_HOME_REGION_BY_NAME))
				statement.setString(1, player.uniqueId.toString())
				statement.setString(2, homeName)
				val resultSet = statement.executeQuery()
				if (resultSet != null && resultSet.next()) {
					val regionName = resultSet.getString("region_id")
					val world = resultSet.getString("world")
					val locX = resultSet.getInt("x")
					val locY = resultSet.getInt("y")
					val locZ = resultSet.getInt("z")
					val regionLocation =
						Location(plugin.server.getWorld(world), locX.toDouble(), locY.toDouble(), locZ.toDouble())
					return Home(player, regionLocation, regionName)
				}
			}
		}

		return null;
	}

	override fun getPlayerHomes(player: Player): List<Home>? {
		TODO("Not yet implemented")
	}

	// Backend

	override fun addPlayerHome(player: Player, region: ProtectedRegion, teleportLocation: Location, homeName: String) {
		sqlDatabase.getConnection().use {
			if (it != null && !it.isClosed()) {
				val statement = it.prepareStatement(processQuery(ADD_PLAYER_HOME_REGION))
				statement.setString(1, player.uniqueId.toString())
				statement.setString(2, region.id)
				statement.setString(3, homeName)
				statement.setInt(4, teleportLocation.blockX)
				statement.setInt(5, teleportLocation.blockY)
				statement.setInt(6, teleportLocation.blockZ)
				statement.setString(7, teleportLocation.world?.name)
				statement.execute()
			}
		}
	}

	override fun getPlayerHomeRegions(player: Player): List<ProtectedRegion>? {
		sqlDatabase.getConnection().use {
			if (it != null && !it.isClosed()) {
				val statement = it.prepareStatement(processQuery(GET_PLAYER_HOME_REGIONS_BY_PLAYER_ID))
				statement.setString(1, player.uniqueId.toString())
				val resultSet = statement.executeQuery()
				if (resultSet != null) {
					val regions = WorldGuard.getInstance().platform.regionContainer
					val homeRegions = generateSequence {
						if (resultSet.next()) {
							val manager = regions.get(BukkitAdapter.adapt(Bukkit.getWorld(resultSet.getString(1))))
							manager?.getRegion(resultSet.getString(2))
						} else null
					}.filterNotNull().toList()
					return if (homeRegions.isNotEmpty()) {
						homeRegions
					} else {
						null
					}
				}
			}
		}
		return null;
	}

	override fun getRegionOwner(region: ProtectedRegion): Player? {
		sqlDatabase.getConnection().use {
			if (it != null && !it.isClosed()) {
				val statement = it.prepareStatement(processQuery(GET_REGION_OWNER))
				statement.setString(1, region.id)
				val resultSet = statement.executeQuery()
				if (resultSet != null) {
					return Bukkit.getPlayer(UUID.fromString(resultSet.getString(1)))
				}
			}
		}
		return null;
	}

	override fun getPlayerHomeRegionByName(player: Player, homename: String): ProtectedRegion? {
		sqlDatabase.getConnection().use {
			if (it != null && !it.isClosed()) {
				val statement = it.prepareStatement(processQuery(GET_PLAYER_HOME_REGION_BY_NAME))
				statement.setString(1, player.uniqueId.toString())
				statement.setString(2, homename)
				val resultSet = statement.executeQuery()
				if (resultSet != null) {
					val regions = WorldGuard.getInstance().platform.regionContainer
					val manager = regions.get(BukkitAdapter.adapt(Bukkit.getWorld(resultSet.getString(1))))
					return manager?.getRegion(resultSet.getString(2))
				}
			}
		}
		return null;
	}
}
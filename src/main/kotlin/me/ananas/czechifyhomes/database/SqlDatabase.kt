package me.ananas.czechifyhomes.database

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.bukkit.plugin.Plugin
import java.sql.Connection

class SqlDatabase(
	val dbUrl: String,
	val table_prefix: String,
	val username: String,
	val password: String,
	val plugin: Plugin
) {

	private val log = plugin.getLogger()

	val hikariConfig = HikariConfig();
	var hikariDatasource = initialiseHikari();

	fun initialiseHikari(): HikariDataSource {
		log.info("Opening a database connection to $dbUrl...")
		hikariConfig.jdbcUrl = dbUrl;
		hikariConfig.username = username;
		hikariConfig.password = password;
		hikariConfig.maximumPoolSize = 3;
		hikariConfig.minimumIdle = 1;
		hikariConfig.leakDetectionThreshold = 60 * 1000
		hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
		hikariConfig.addDataSourceProperty("prepStmtCacheSize", "100")
		hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "1024")
		return HikariDataSource(hikariConfig)
	}

	fun isConnected(): Boolean {
		return !hikariDatasource.isClosed
	}

	fun isRunning(): Boolean {
		return hikariDatasource.isRunning
	}

	fun getConnection(): Connection? {
		if (isConnected() && isRunning()) {
			return hikariDatasource.connection
		}
		return null
	}

	fun disconnect() {
		if (isConnected()) {
			hikariDatasource.close()
		}
	}
}
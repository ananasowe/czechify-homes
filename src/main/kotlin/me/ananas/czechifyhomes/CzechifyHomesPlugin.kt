package me.ananas.czechifyhomes

import me.ananas.czechifyhomes.commands.*
import me.ananas.czechifyhomes.database.DataStorage
import me.ananas.czechifyhomes.database.SqlDataStorage
import me.ananas.czechifyhomes.database.SqlDatabase
import me.ananas.czechifyhomes.events.PostponedActionEvent
import me.ananas.czechifyhomes.listeners.PostponedActionListener
import me.ananas.czechifyhomes.modules.SpawnModule
import me.ananas.czechifyhomes.services.PostponedActionType
import me.ananas.czechifyhomes.services.PostponedActionsQueue
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

class CzechifyHomesPlugin : JavaPlugin() {
    private val log = getLogger()
    val i18n = I18nManager(this)
    lateinit var dataStorage: DataStorage
    lateinit var postponedActionsQueue: PostponedActionsQueue
    lateinit var spawnModule: SpawnModule

    override fun onEnable() {
        loadConfiguration()
        loadActionQueue()
        /*val cmdGraph = BasicBukkitCommandGraph()
        cmdGraph.rootDispatcherNode.registerCommands()
        BukkitIntake(this, cmdGraph).register()*/
        getCommand("sethome")?.setExecutor(SethomeCommand(this))
        getCommand("home")?.setExecutor(HomeCommand(this))
        getCommand("tpa")?.setExecutor(TpaCommand(this))
        getCommand("tpaccept")?.setExecutor(TpacceptCommand(this))
        getCommand("tpdeny")?.setExecutor(TpdenyCommand(this))
        //val cmdGraph = BasicBukkitCommandGraph()
        //cmdGraph.rootDispatcherNode.registerCommands(HomeCommands())
        //val homeCommandNode = cmdGraph.rootDispatcherNode.registerNode("home")
        //homeCommandNode.registerCommands(HomeCommands())
        //BukkitIntake(this, cmdGraph).register()
        server.pluginManager.registerEvents(PostponedActionListener(this), this)
        // server.pluginManager.registerEvents(PlayerMoveListener(this), this) // Agressive method, potentially resource heavy
        server.pluginManager.callEvent(PostponedActionEvent(Date().time, null, PostponedActionType.OTHER, { }))
    }

    override fun onDisable() {
        dataStorage.shutdown()
        i18n.clearCache()
    }

    fun loadConfiguration() {
        log.info("Loading database configuration...")
        Config.registerConfig("database", "database.yml", this)
        Config.registerConfig("spawn", "spawn.yml", this)
        Config.loadAll()
        loadDBConfiguration()
        loadSpawnConfiguration()
    }

    fun loadDBConfiguration() {
        val databaseConfig = Config.getConfig("database")
        if (databaseConfig != null) {
            if (this::dataStorage.isInitialized) {
                dataStorage.shutdown() // Make sure the database is not connected
            }
            dataStorage = SqlDataStorage(
                SqlDatabase(
                    "jdbc:mysql://" + databaseConfig.getString("host", "localhost").orEmpty()
                            + "/" + databaseConfig.getString("database", "minecraft").orEmpty(),
                    databaseConfig.getString("table_prefix", "czechifyhomes_").orEmpty(),
                    databaseConfig.getString("username", "minecraft").orEmpty(),
                    databaseConfig.getString("password", "minecraft").orEmpty(),
                    this
                ),
                this
            )
            dataStorage.prepare()
        } else {
            log.severe("Database configuration is incorrect!")
        }
    }

    fun loadSpawnConfiguration() {
        val spawnConfig = Config.getConfig("spawn")
        if (spawnConfig != null && spawnConfig.isSet("spawn-module") && spawnConfig.isBoolean("spawn-module")) {
            if (spawnConfig.getBoolean("spawn-module")) {
                spawnModule = SpawnModule(this, spawnConfig)
                spawnModule.init()
            } else {
                log.info("Spawn module will be disabled")
            }
        } else {
            log.severe("Could not load spawn configuration! Check the file or remove the configuration for it to be regenerated!")
        }
    }

    fun loadActionQueue() {
        postponedActionsQueue = PostponedActionsQueue(this)
    }
}

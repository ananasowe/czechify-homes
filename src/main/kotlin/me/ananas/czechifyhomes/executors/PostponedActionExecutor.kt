package me.ananas.czechifyhomes.executors

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import org.bukkit.event.Event
import org.bukkit.event.Listener
import org.bukkit.plugin.EventExecutor

class PostponedActionExecutor(plugin: CzechifyHomesPlugin) : EventExecutor, Listener {
	private val log = plugin.getLogger()
	override fun execute(listener: Listener, event: Event) {
		log.info("Event has been received! (" + event.javaClass.toGenericString() + ")")
	}
}
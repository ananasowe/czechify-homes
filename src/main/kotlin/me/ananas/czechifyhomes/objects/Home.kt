package me.ananas.czechifyhomes.objects

import com.sk89q.worldedit.bukkit.BukkitAdapter
import com.sk89q.worldguard.WorldGuard
import com.sk89q.worldguard.protection.flags.Flags
import com.sk89q.worldguard.protection.regions.ProtectedRegion
import org.bukkit.Location
import org.bukkit.entity.Player

class Home(val player: Player, val regionLocation: Location, val regionId: String) {
	fun getWorldGuardLocation(): com.sk89q.worldedit.util.Location {
		return BukkitAdapter.adapt(regionLocation)
	}

	fun getWorldGuardWorld(): com.sk89q.worldedit.world.World {
		return BukkitAdapter.adapt(regionLocation.world)
	}

	fun getWorldGuardRegion(): ProtectedRegion? {
		val regions = WorldGuard.getInstance().platform.regionContainer.get(BukkitAdapter.adapt(regionLocation.world))
		if (regions != null) {
			return regions.getRegion(regionId);
		}
		return null
	}

	fun getTeleportLocation(): Location? {
		val regions = WorldGuard.getInstance().platform.regionContainer.get(BukkitAdapter.adapt(regionLocation.world))
		if (regions != null) {
			val wgLocation = regions.getRegion(regionId)?.getFlag(Flags.TELE_LOC);
			if (wgLocation != null) {
				return Location(
					regionLocation.world,
					wgLocation.x,
					wgLocation.y,
					wgLocation.z,
					wgLocation.yaw,
					wgLocation.pitch
				)
			}
		}
		return null
	}
}
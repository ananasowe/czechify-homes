package me.ananas.czechifyhomes.modules

import me.ananas.czechifyhomes.Config
import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.commands.SetSpawnCommand
import me.ananas.czechifyhomes.commands.SpawnCommand
import org.bukkit.Location

class SpawnModule(val plugin: CzechifyHomesPlugin, val config: Config.RConfig) {
	private val log = plugin.getLogger()
	private lateinit var spawnLocation: Location

	fun init() {
		val entries = arrayOf("x", "y", "z", "yaw", "pitch", "world")
		entries.forEach { entry ->
			if (!config.isSet("loc.$entry")) {
				log.severe("Could not load spawn configuration! Make sure the spawn.yml is formatted correctly or remove it to generate a new one!")
				return
			}
		}
		val spawnWorld = plugin.server.getWorld(config.getString("loc.world", "world")!!)
		val spawnX = config.getDouble("loc.x")
		val spawnY = config.getDouble("loc.y")
		val spawnZ = config.getDouble("loc.z")
		val spawnYaw = config.getDouble("loc.yaw")
		val spawnPitch = config.getDouble("loc.pitch")
		spawnLocation = Location(spawnWorld, spawnX, spawnY, spawnZ, spawnYaw.toFloat(), spawnPitch.toFloat())

		plugin.getCommand("spawn")?.setExecutor(SpawnCommand(plugin, this))
		plugin.getCommand("setspawn")?.setExecutor(SetSpawnCommand(plugin, this))
		log.info("Enabled spawn module!")
	}

	fun updateSpawnLocation(location: Location) {
		spawnLocation = location
		config.set("loc.x", location.x)
		config.set("loc.y", location.y)
		config.set("loc.z", location.z)
		config.set("loc.yaw", location.yaw)
		config.set("loc.pitch", location.pitch)
		config.save()
	}

	fun getSpawnLocation(): Location {
		return spawnLocation
	}
}
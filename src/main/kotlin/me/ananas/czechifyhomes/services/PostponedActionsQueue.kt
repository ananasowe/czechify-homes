package me.ananas.czechifyhomes.services

import me.ananas.czechifyhomes.CzechifyHomesPlugin
import me.ananas.czechifyhomes.events.PostponedActionEvent
import org.bukkit.scheduler.BukkitRunnable
import java.util.*
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.ConcurrentSkipListMap

class PostponedActionsQueue(plugin: CzechifyHomesPlugin) : BukkitRunnable() {
	private val log = plugin.getLogger()
	private val events: ConcurrentMap<Long, PostponedActionEvent> = ConcurrentSkipListMap()

	init {
		this.runTaskTimer(plugin, 20, 20)
	}

	override fun run() {
		synchronized(events) {
			if (events.size > 0) {
				val unixTime = Date().time
				val closestTime = events.keys.first()
				if (unixTime > closestTime) {
					val event = events[closestTime]
					if (event != null) {
						try {
							event.executeAction()
						} catch (exception: Exception) {
							log.severe("There was a problem executing a scheduled event (" + unixTime + ")!" + " The following error occurred: " + exception.localizedMessage)
						} finally {
							events.remove(closestTime)
							log.info(
								"Executed a waiting event (ID: " + closestTime +
										", DELTA: " + "%.2f".format((closestTime - event.createdAt) / 1000.0) +
										" sec.). Current queue size: " + events.size
							)
						}
					}
				}
			}
		}
	}

	fun queue(time: Long, postponedActionEvent: PostponedActionEvent) {
		var plannedTime = time
		synchronized(events) {
			while (events.put(plannedTime, postponedActionEvent) != null) { // Search for an empty slot in the queue
				plannedTime++
			}
		}
	}

	fun hasEventsAssociatedWithUUID(uuid: UUID) {
		hasEventsOFTypeAssociatedWithUUID(uuid, null)
	}

	fun hasEventsOFTypeAssociatedWithUUID(uuid: UUID, postponedActionTypes: Array<PostponedActionType>?): Boolean {
		return getOldestEventOfTypeAssociatedWithUUID(uuid, postponedActionTypes, false) != null // True when found
	}

	fun getOldestEventOfTypeAssociatedWithUUID(
		uuid: UUID,
		postponedActionTypes: Array<PostponedActionType>?,
		removeWhenFound: Boolean
	): PostponedActionEvent? {
		synchronized(events) {
			for (event in events) {
				val postponedActionEvent = event.value
				if (postponedActionEvent.participents?.first() == uuid &&
					postponedActionTypes?.contains(postponedActionEvent.postponedActionType) != false
				) {
					if (removeWhenFound) {
						events.remove(event.key)
					}
					return postponedActionEvent
				}
			}
		}
		return null;
	}

	fun cancelActionsForFirstAssociatedUUID(uuid: UUID, postponedActionType: PostponedActionType): Int {
		var count = 0
		synchronized(events) {
			events.forEach { time, postponedActionEvent ->
				if (postponedActionEvent.participents?.first() == uuid &&
					postponedActionEvent.postponedActionType == postponedActionType
				) {
					events.remove(time)
					count++
				}
			}
		}
		return count
	}
}
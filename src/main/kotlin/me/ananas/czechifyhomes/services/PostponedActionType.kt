package me.ananas.czechifyhomes.services

enum class PostponedActionType {
	TELEPORT, TELEPORT_REQUEST, OTHER
}